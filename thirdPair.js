const obj = {
    name: 'Eugene',
    lastName: 'Dudko',
    age: 28,
    growth: 182,

}

let beta = '';
let delta = 0;
for (key in obj) {
  delta = delta +obj[key] 
    if (delta === '') {
        delta = obj[key];
    }
    beta = beta + obj[key];
    delete obj[key];
        }
    

//--2

obj[delta] = beta;

//--3

const cop = { ...obj };

"aav" === cop ? console.log(true) : console.log(false);

//--4

const arra = [99, 100, 101];
const arrb = [98, 97];
const arrc = [arra[0] + arrb[0], ...arra, ...arrb, arra[arra.length - 1] + arrb[arrb.length - 1]];

//--5

let max = 0;
let min = 0;

for (key in arrc) {
    if (arrc[min] > arrc[key]) {
        min = key;
    }
    if (arrc[max] < arrc[key]) {
        max = key;
    }
}

[arrc[max], arrc[min]] = [arrc[min], arrc[max]];

